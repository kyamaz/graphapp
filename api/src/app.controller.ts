import { filter, tap, map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import { AuthPayload } from './app.model';
import {
  Controller,
  Post,
  Request,
  Body,
  HttpException,
  HttpStatus,
  Res,
} from '@nestjs/common';
import { AppService } from './app.service';
import { Response } from 'express';

@Controller('api/auth')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('signin')
  signin(@Body() auth: AuthPayload, @Res() res: Response): any {
    return this.appService.signin(auth).pipe(
      catchError((e: any) => {
        throw new HttpException('Forbidden', HttpStatus.UNAUTHORIZED);
      }),
      map((r: any) =>
        res
          .status(HttpStatus.OK)
          .header({ status: 200, 'Access-Control-Allow-Origin': '*' })
          .json(r),
      ),
    );
  }

  @Post('refresh-token')
  refresh(@Body() auth: AuthPayload, @Res() res: Response): any {
    return this.appService.refresh().pipe(
      map((r: any) =>
        res
          .status(HttpStatus.OK)
          .header({ status: 200, 'Access-Control-Allow-Origin': '*' })
          .json(r),
      ),
    );
  }
}

import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { MD5 } from './utils';
import { CONST } from './app.model';

function validateToken(tk: string): boolean {
  return MD5(tk) === CONST.expectedToken;
}
@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const tk: InstanceType<any> = request.headers.authorization;
    return validateToken(tk);
  }
}

import * as mongoose from 'mongoose';

export const TodosSchema = new mongoose.Schema({
  title: String,
  description: String,
});

import { Document } from 'mongoose';
export interface Todos extends Document {
  readonly title: string;
  readonly description: string;
}

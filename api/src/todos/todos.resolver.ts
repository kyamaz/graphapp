import { TodosType } from './create-todos.dto';
import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { TodosService } from './todos.service';
import { TodosInput } from './input-todos.input';

@Resolver('Todos')
export class TodosResolver {
  constructor(private readonly TodosService: TodosService) {}

  @Query(() => [TodosType])
  async todos(): Promise<TodosType[]> {
    return this.TodosService.findAll();
  }

  @Mutation(() => TodosType)
  async createItem(@Args('input') input: TodosInput): Promise<TodosInput> {
    return this.TodosService.create(input);
  }

  @Mutation(() => TodosType)
  async updateItem(
    @Args('id') id: string,
    @Args('input') input: TodosInput,
  ): Promise<TodosInput> {
    return this.TodosService.update(id, input);
  }

  @Mutation(() => TodosType)
  async deleteItem(@Args('id') id: string): Promise<TodosInput> {
    return this.TodosService.delete(id);
  }

  @Query(() => String)
  async hello() {
    return 'hello';
  }
}

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Todos } from './todos.interface';
import { TodosInput } from './input-todos.input';
import { TodosType } from './create-todos.dto';
@Injectable()
export class TodosService {
  constructor(@InjectModel('Todos') private itemModel: Model<Todos>) {}

  async create(createTodosDto: TodosInput): Promise<TodosType> {
    const createdItem = new this.itemModel(createTodosDto);
    return await createdItem.save();
  }

  async findAll(): Promise<TodosType[]> {
    return await this.itemModel.find().exec();
  }

  async findOne(id: string): Promise<TodosType> {
    return await this.itemModel.findOne({ _id: id });
  }

  async delete(id: string): Promise<TodosType> {
    return await this.itemModel.findByIdAndRemove(id);
  }

  async update(id: string, todos: Todos): Promise<TodosType> {
    return await this.itemModel.findByIdAndUpdate(id, todos, { new: true });
  }
}

import { MongooseModule } from '@nestjs/mongoose';
import { TodosSchema } from './todos.schema';
import { Module } from '@nestjs/common';
import { TodosService } from './todos.service';
import { TodosResolver } from './todos.resolver';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Todos', schema: TodosSchema }]),
  ],
  providers: [TodosService, TodosResolver],
})
export class TodosModule {}

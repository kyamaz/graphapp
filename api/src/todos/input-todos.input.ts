import { InputType, Field, Int, ID } from 'type-graphql';

@InputType()
export class TodosInput {
  @Field()
  readonly title: string;

  @Field()
  readonly description: string;
}

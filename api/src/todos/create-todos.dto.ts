import { ObjectType, Field, ID } from 'type-graphql';

@ObjectType()
export class TodosType {
  @Field(() => ID)
  readonly id?: string;
  @Field()
  readonly title: string;
  @Field()
  readonly description: string;
}
